!create u1:User
!set u1.id := 1
!set u1.name := 'User 1'

!create u2:User
!set u2.id := 2
!set u2.name := 'User 2'

!create u3:User
!set u3.id := 3
!set u3.name := 'User 3'

!create uFactory:UserFactory
!insert (uFactory, u1) into AllUsers
!insert (uFactory, u2) into AllUsers
!insert (uFactory, u3) into AllUsers

!create g1:GameState
!set g1.id := 1
!set g1.name := 'Game 1'
!set g1.activePlayerID := u1.id

!create g2:GameState
!set g2.id := 2
!set g2.name := 'Game 2'
!set g2.activePlayerID := u3.id

!create gFactory:GameFactory
!insert (gFactory, g1) into AllGames
!insert (gFactory, g2) into AllGames

!create g1DrawPile:Deck
!create g1DiscardPile:Deck

!insert (g1, g1DrawPile) into DrawDiscardPile
!insert (g1, g1DiscardPile) into DrawDiscardPile

!create g2DrawPile:Deck
!create g2DiscardPile:Deck

!insert (g2, g2DrawPile) into DrawDiscardPile
!insert (g2, g2DiscardPile) into DrawDiscardPile

!create p1:Player
!create p1Hand:Hand
!create p1Phase:Phase
!create p1SetOf3:ValueSet
!create p1SetOf3Hand:Hand

!insert (u1, p1) into UserPlays
!insert (g1, p1) into GameMembers
!insert (p1, p1Hand) into PlayerHand
!insert (p1, p1Phase) into CurrentPhase
!insert (p1Phase, p1SetOf3) into PhaseMatches
!insert (p1SetOf3, p1SetOf3Hand) into MatchHand

!create p2:Player
!create p2Hand:Hand
!create p2Phase:Phase
!create p2SameColor7:ColorSet
!create p2SameColor7Hand:Hand

!insert (u2, p2) into UserPlays
!insert (g1, p2) into GameMembers
!insert (p2, p2Hand) into PlayerHand
!insert (p2, p2Phase) into CurrentPhase
!insert (p2Phase, p2SameColor7) into PhaseMatches
!insert (p2SameColor7, p2SameColor7Hand) into MatchHand

!create p3:Player
!create p3Hand:Hand
!create p3Phase:Phase
!create p3RunOf7:Run
!create p3RunOf7Hand:Hand

!insert (u3, p3) into UserPlays
!insert (g2, p3) into GameMembers
!insert (p3, p3Hand) into PlayerHand
!insert (p3, p3Phase) into CurrentPhase
!insert (p3Phase, p3RunOf7) into PhaseMatches
!insert (p3RunOf7, p3RunOf7Hand) into MatchHand

!create red1:Card
!set red1.color := #RED
!set red1.value := 1
!insert (g1DrawPile, red1) into CardDeck

!create red2:Card
!set red2.color := #RED
!set red2.value := 2
!insert (g1DiscardPile, red2) into CardDeck

!create w1:Card
!set w1.color := #WILD
!insert (p1Hand, w1) into CardDeck

!create skip1:Card
!set skip1.color := #SKIP
!insert (p2Hand, skip1) into CardDeck

!create red3:Card
!set red3.color := #RED
!set red3.value := 3
!insert (p1SetOf3Hand, red3) into CardDeck

!create green3:Card
!set green3.color := #GREEN
!set green3.value := 3
!insert (p1SetOf3Hand, green3) into CardDeck

!create w2:Card
!set w2.color := #WILD
!insert (p1SetOf3Hand, w2) into CardDeck

!create blue3:Card
!set blue3.color := #BLUE
!set blue3.value := 3
!insert (p1SetOf3Hand, blue3) into CardDeck

!create blue4:Card
!set blue4.color := #BLUE
!set blue4.value := 4
!insert (p2SameColor7Hand, blue4) into CardDeck

!create blue7:Card
!set blue7.color := #BLUE
!set blue7.value := 7
!insert (p2SameColor7Hand, blue7) into CardDeck

!create w3:Card
!set w3.color := #WILD
!insert (p2SameColor7Hand, w3) into CardDeck

!create blue1:Card
!set blue1.color := #BLUE
!set blue1.value := 1
!insert (p2SameColor7Hand, blue1) into CardDeck

!create yellow1:Card
!set yellow1.color := #YELLOW
!set yellow1.value := 1
!insert (p3RunOf7Hand, yellow1) into CardDeck

!create green2:Card
!set green2.color := #GREEN
!set green2.value := 2
!insert (p3RunOf7Hand, green2) into CardDeck

!create yellow3:Card
!set yellow3.color := #YELLOW
!set yellow3.value := 3
!insert (p3RunOf7Hand, yellow3) into CardDeck

!create red4:Card
!set red4.color := #RED
!set red4.value := 4
!insert (p3RunOf7Hand, red4) into CardDeck

!create green5:Card
!set green5.color := #GREEN
!set green5.value := 5
!insert (p3RunOf7Hand, green5) into CardDeck

!create w4:Card
!set w4.color := #WILD
!insert (p3RunOf7Hand, w4) into CardDeck

!create yellow7:Card
!set yellow7.color := #YELLOW
!set yellow7.value := 7
!insert (p3RunOf7Hand, yellow7) into CardDeck

!create w5:Card
!set w5.color := #WILD
!insert (p3RunOf7Hand, w5) into CardDeck

!create green9:Card
!set green9.color := #GREEN
!set green9.value := 9
!insert (p3RunOf7Hand, green9) into CardDeck

!create blue10:Card
!set blue10.color := #BLUE
!set blue10.value := 10
!insert (p3RunOf7Hand, blue10) into CardDeck

!create red11:Card
!set red11.color := #RED
!set red11.value := 11
!insert (p3RunOf7Hand, red11) into CardDeck

!create yellow12:Card
!set yellow12.color := #YELLOW
!set yellow12.value := 12
!insert (p3RunOf7Hand, yellow12) into CardDeck